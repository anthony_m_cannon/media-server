package com.mediaplus.server;

import com.mediaplus.server.util.Constants;

import uk.co.caprica.vlcj.discovery.NativeDiscovery;
import uk.co.caprica.vlcj.logger.Logger;

public class Main {

	public static void main(String args[]){
        Logger.setLevel(Logger.Level.INFO);

        NativeDiscovery vlcFinder = new NativeDiscovery();
        boolean vlcFound = vlcFinder.discover();
        if(!vlcFound){
            System.err.println("VLC not found.");
            System.err.println("Make sure that you have a " + System.getProperty("sun.arch.data.model") + " bit version of VLC installed");
            System.err.println("http://www.videolan.org/vlc/index.en_GB.html#download");
            System.exit(1);
        }

        Server server = new Server(Constants.SERVER_PORT);
        server.start();

    }
}
