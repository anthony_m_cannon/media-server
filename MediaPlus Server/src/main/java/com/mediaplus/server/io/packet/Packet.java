package com.mediaplus.server.io.packet;

import com.mediaplus.server.client.Client;
import com.mediaplus.server.util.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;

/**
 * Created by Deividas on 06/02/2015.
 */
public class Packet {

    protected int id;
    protected String data;

    public enum PacketType{
        ERROR(0x0),
        HANDSHAKE(0x1),
        COMMAND(0x2);

        int id;

        PacketType(int id){
            this.id = id;;
        }
        public static PacketType getPacketType(int id){
            switch(id){
                default:
                    return ERROR;
                case 0x0:
                    return ERROR;
                case 0x1:
                    return HANDSHAKE;
                case 0x2:
                    return COMMAND;
            }
        }

        public int getId() {
            return id;
        }
    }

    public Packet(int id, String data){
        this.id = id;
        this.data = data;
    }

    public byte[] buildPacket(){
        String packetString = "";
        if(id < 10){
            packetString += "0";
        }
        packetString += id;
        packetString += data;
        packetString += Constants.END_OF_INPUT;

        System.out.println("Built new packet: " + packetString);

        return packetString.getBytes();
    }

    public static ArrayList<Packet> readPacket(InputStream in, Client client) {
        ArrayList<Packet> packetBuffer = new ArrayList<Packet>();
        try{
            byte[] packetBytes = new byte[1024];
            while(in.read(packetBytes) != -1){
                String packetStr = new String(packetBytes, "UTF-8");
                String[] subPackets = packetStr.split(String.valueOf(Constants.END_OF_INPUT));
                for(int i = 0; i < subPackets.length;i++){
                    String subPacketStr = subPackets[i];
                    String packetIdStr = packetStr.substring(0, 2);
                    int packetId = Integer.parseInt(packetIdStr);
                    PacketType packetType = PacketType.getPacketType(packetId);
                    String packetData = "";
                    if(packetStr.length() > 2){
                        packetData = subPacketStr.substring(2);
                    }
                    packetData = packetData.trim();
                    packetData.replaceAll(Constants.END_OF_INPUT + "", "");
                    switch (packetType) {
                        case HANDSHAKE:
                            packetBuffer.add(new HandshakePacket(packetId, packetData));
                            break;
                        case COMMAND:
                            packetBuffer.add(new CommandPacket(packetData));
                            break;
                    }
                }
                return packetBuffer;
            }
        }catch(SocketException sockE){
            if(sockE.getMessage().equalsIgnoreCase("connection reset")){
                client.disconnect();

            }
        }catch(IOException e){
            e.printStackTrace();
        }

        return packetBuffer;
    }
    /*
    public static Packet[] readPacket(InputStream in, Client client) {
        try {
            ArrayList<Packet> packetBuffer = new ArrayList<Packet>();
            //allocate byte buffer
            byte[] packetBytes = new byte[1024];
            while(in.read(packetBytes) != -1){
                String packetStr = new String(packetBytes, "UTF-8");
                String packetIdStr = packetStr.substring(0, 2);
                int packetId = Integer.parseInt(packetIdStr);
                PacketType packetType = PacketType.getPacketType(packetId);
                String packetData = "";
                if(packetStr.length() > 2){
                    packetData = packetStr.substring(2);
                }
                packetData = packetData.trim();
                switch (packetType) {
                    case HANDSHAKE:
                        packetBuffer.add(new HandshakePacket(packetId, packetData));
                        break;
                    case COMMAND:
                        return new CommandPacket(packetData);
                }
            }
        }catch(SocketException sockE){
            if(sockE.getMessage().equalsIgnoreCase("connection reset")){
                client.disconnect();

            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }*/



    public synchronized boolean sendPacket(OutputStream out){
        try {
            out.write(buildPacket());
            out.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
