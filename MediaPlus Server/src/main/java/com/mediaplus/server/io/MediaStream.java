package com.mediaplus.server.io;

import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

import com.mediaplus.server.Server;
import com.mediaplus.server.util.Constants;

public class MediaStream {

    public void startStream() {
        String media = Constants.MOVIE_ROOT_DIR + "\\test.mp4";
        String options = formatHttpStream(Server.getServerIP(), Constants.STREAM_PORT);

        MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
        HeadlessMediaPlayer mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();
        mediaPlayer.playMedia(media, options);
    }

    private String formatHttpStream(String serverAddress, int serverPort) {
        StringBuilder sb = new StringBuilder(60);
        sb.append(":sout=#duplicate{dst=std{access=http,mux=ts,");
        sb.append("dst=");
        sb.append(serverAddress);
        sb.append(':');
        sb.append(serverPort);
        sb.append("}}");
        return sb.toString();
    }
}