package com.mediaplus.server.io.packet;

import com.mediaplus.server.client.ClientType;

/**
 * Created by Deividas on 06/02/2015.
 */
public class HandshakePacket extends Packet {

   private int pin;

    public HandshakePacket(int id, String data) {
        super(id, data);
    }

    public ClientType getClientType(){

        int clientType = Integer.parseInt(data);
        switch(clientType){
            case 0x0:
                return ClientType.UNKNOWN;
            case 0x1:
                return ClientType.DESKTOP;
            case 0x2:
            return ClientType.ANDROID;
            default:
                return ClientType.UNKNOWN;
        }
    }

}
