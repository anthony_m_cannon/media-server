package com.mediaplus.server.io.packet;

/**
 * Created by Deividas on 04/03/2015.
 */
public class CommandPacket extends Packet {

    private int controlCommand;
    private String controlParameter;

    public CommandPacket(String packetData) {
        super(PacketType.COMMAND.id, packetData);
        System.out.println("packet data: " + packetData);
        if(packetData.length() > 2) {
            controlCommand = Integer.parseInt(packetData.substring(0, 2));
            controlParameter = packetData.substring(2, packetData.length());
        }else{
            if(packetData.equals("")) return;
            controlCommand = Integer.parseInt(packetData.substring(0, packetData.length()));
        }
    }

    public int getCommandId() {
        return controlCommand;
    }

    public String getCommandData() {
        return controlParameter;
    }
}
