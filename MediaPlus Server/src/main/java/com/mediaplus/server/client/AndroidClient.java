package com.mediaplus.server.client;

import com.mediaplus.server.BaseObject;
import com.mediaplus.server.io.packet.CommandPacket;
import com.mediaplus.server.io.packet.CommandProperty;
import com.mediaplus.server.io.packet.CommandType;
import com.mediaplus.server.io.packet.Packet;
import com.mediaplus.server.util.Constants;
import uk.co.caprica.vlcj.filter.MediaFileFilter;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;

/**
 * Created by Deividas on 02/03/2015.
 */
public class AndroidClient extends ClientHandler implements RemoteController {

    private int connectionPin;
    private DesktopClient controlClient;

    public AndroidClient(Client client) {
        super(client);

    }

    @Override
    public boolean handleClient() {
        
        ArrayList<Packet> packets = Packet.readPacket(in, client);
        for (int i = 0; i < packets.size(); i++) {
            Packet packet = packets.get(i);
            if(packet instanceof CommandPacket){
                CommandPacket commandPacket = (CommandPacket) packet;
                handleCommand(commandPacket);
            }
        }

        return false;
    }

    private void handleCommand(CommandPacket commandPacket) {
        try {
            int commandId = commandPacket.getCommandId();
            CommandType type = CommandType.getCommandType(commandId);
            HeadlessMediaPlayer mediaPlayer = null;
            if (type != CommandType.BE_REMOTE) {
                if (controlClient == null) return;
                mediaPlayer = controlClient.getMediaPlayer();
            }

            switch (type) {
                case PLAY_PAUSE: //play/pause
                    controlClient.playPause();
                    break;
                case STOP://stop
                    controlClient.stop();
                    break;
                case LEFT://left
                    controlClient.left();
                    break;
                case RIGHT://right
                    controlClient.right();
                    break;
                case UP://up
                    controlClient.up();
                    break;
                case DOWN://down
                    controlClient.down();
                    break;
                case ENTER://enter
                    controlClient.enter();
                    break;
                case BACK://back
                    controlClient.back();
                    break;
                case INFO://info
                    controlClient.info();
                    break;
                case REPEAT://repeat
                    controlClient.repeat();
                    break;
                case SHUFFLE://shuffle
                    controlClient.shuffle();
                    break;
                case SET_PROPERTY://volume level (+ level)
                    int propertyId = Integer.parseInt(commandPacket.getCommandData().substring(0, 2));
                    String propertyData = commandPacket.getCommandData().substring(2);
                    CommandProperty property = CommandProperty.getProperty(propertyId);

                    switch (property) {
                        case ERROR:

                            break;
                        case VOLUME:
                            controlClient.volume(Integer.parseInt(propertyData));
                            break;
                    }

                    System.out.println("data: " + commandPacket.getCommandData());

                    break;
                case GET_PROPERTY://film position (+ position)
                    int position = Integer.parseInt(commandPacket.getCommandData());
                    controlClient.position(position);
                    break;
                case MUTE://mute
                    controlClient.mute();
                    break;
                case POWER://power
                    controlClient.power();
                    break;
                case STREAM_FILM://stream film (+ name)
                    String name = commandPacket.getCommandData();
                    controlClient.stream(name);
                    break;
                case GET_FILMS: //get films
                    sendFilms();
                    break;
                case BE_REMOTE://be remote
                    int pin = Integer.parseInt(commandPacket.getCommandData());
                    if (setControlClient(pin)) {
                        CommandPacket pinResponse = new CommandPacket(CommandType.BE_REMOTE.getId() + "1");
                        pinResponse.sendPacket(out);
                    } else {
                        CommandPacket pinResponse = new CommandPacket(CommandType.BE_REMOTE.getId() + "0");
                        pinResponse.sendPacket(out);
                    }
                    break;

            }
        }catch(Exception e){
            System.err.println("Could not process packet");
        }
    }

    public boolean setControlClient(int pin) {
        ArrayList<Client> clients = BaseObject.server.getClients();
        for(Client c : clients){
            if(c.getHandler() instanceof DesktopClient){
                DesktopClient d = (DesktopClient) c.getHandler();
                if(d.getPin() == pin){
                    controlClient = d;
                    connectionPin = pin;
                    return true;
                }
            }
        }
        return false;
    }

    public DesktopClient getControlClient() {
        return controlClient;
    }

    public void setControlClient(DesktopClient controlClient) {
        this.controlClient = controlClient;
    }

    private void sendFilms() {
        File[] films = getFilms();
        for(int i = 0; i < films.length;i++){
            CommandPacket filmCommand = new CommandPacket(CommandType.GET_FILMS.getId() + films[i].getName());
            filmCommand.sendPacket(out);
        }
    }

    private File[] getFilms(){
        File folder = new File(Constants.MOVIE_ROOT_DIR);
        FileFilter filter = new MediaFileFilter();
        return folder.listFiles(filter);
    }

}
