package com.mediaplus.server.client;

import com.mediaplus.server.BaseObject;
import com.mediaplus.server.Server;
import com.mediaplus.server.io.packet.*;
import com.mediaplus.server.util.Constants;
import com.mediaplus.server.util.Util;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Deividas on 02/03/2015.
 */
public class DesktopClient extends ClientHandler{

    private final HeadlessMediaPlayer mediaPlayer;
    private int pin;
    private ArrayList<RemoteController> remoteControllers;
    private static int desktopId = -1;

    public DesktopClient(Client client) {
        super(client);

        MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
        mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();

        pin = Util.generateNumber(1000, 9999);
        while(!isPinUnique(pin)){
            pin = Util.generateNumber(1000, 9999);
        }

        HandshakePacket handshakePacket = new HandshakePacket(Packet.PacketType.HANDSHAKE.getId(), pin + "");
        handshakePacket.sendPacket(out);

        remoteControllers = new ArrayList<RemoteController>();

        desktopId++;

    }

    Random r = new Random();

    @Override
    public boolean handleClient() {
        ArrayList<Packet> packets = Packet.readPacket(in, client);

        for(int i = 0 ;i < packets.size();i++) {
            Packet packet = packets.get(i);
            if (packet instanceof CommandPacket) {
                CommandPacket commandPacket = (CommandPacket) packet;
                int id = commandPacket.getCommandId();
                CommandType command = CommandType.getCommandType(id);
                switch (command) {
                    case GET_PROPERTY:
                        int property = Integer.parseInt(commandPacket.getCommandData());
                        CommandProperty commandProperty = CommandProperty.getProperty(property);
                        switch (commandProperty) {
                            case PLAY_TIME:
                                System.out.println("PLAY TIME " + mediaPlayer.getTime());
                                break;
                        }
                        break;
                }
            }
        }

        return true;
    }
    
    private boolean isPinUnique(int pin){
        ArrayList<Client> clients = BaseObject.server.getClients();
        for (Client c : clients) {
            if(c.getHandler() instanceof DesktopClient){
                DesktopClient handler = (DesktopClient) c.getHandler();
                if(handler.getPin() == pin){
                    return false;
                }
            }
        }

        return true;
    }

    public int getPin() {
        return pin;
    }

    private String streamMedia(String path){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }

        String streamMrl = Server.getServerIP() + ":" + (Constants.STREAM_PORT + desktopId);

        String options = ":sout=#duplicate{dst=std{access=http,mux=ts,dst=";
        options += streamMrl;
        options += "}}";
        System.out.println("attempting to play: " + Constants.MOVIE_ROOT_DIR + "\\" + path);
        mediaPlayer.playMedia(Constants.MOVIE_ROOT_DIR + "\\" + path, options);
        System.out.println("Started stream on : " + Server.getServerIP() + ":" + Constants.STREAM_PORT);

        return streamMrl;
    }

    public HeadlessMediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void sendCommand(int commandId){
        sendCommand(commandId, "");
    }

    public void sendCommand(CommandType type){
        sendCommand(type.getId(), "");
    }


    public void sendCommand(int commandId, String commandData){
        String id = "";
        if(commandId < 10){
            id += 0;
        }
        id += commandId;
        CommandPacket commandPacket = new CommandPacket(id + commandData);
        System.out.println("Sending command " + CommandType.getCommandType(commandId) + " with [" + commandData + "]");
        commandPacket.sendPacket(out);
    }

    public void playPause(){
        sendCommand(CommandType.PLAY_PAUSE);
    }

    public void stop() {
        mediaPlayer.stop();
        sendCommand(CommandType.STOP);
    }

    public void left() {

    }

    public void right() {
        
    }

    public void up() {
    }

    public void down() {
    }

    public void enter() {
    }

    public void back() {
    }

    public void info() {
    }

    public void repeat() {
        if(mediaPlayer.getRepeat()){
            mediaPlayer.setRepeat(false);
        }else {
            mediaPlayer.setRepeat(true);
        }
    }

    public void shuffle() {
        //TODO: implement a queue
    }

    public void volume(int volume) {
        //1301[volume]
        sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.VOLUME.getId() + volume + "");
    }

    public void position(int position) {
        mediaPlayer.setPosition(position);
        //TODO: send position command to desktop client
    }

    public void mute(){
        sendCommand(CommandType.MUTE);
    }

    public void power() {

    }

    public void stream(String name) {
        String mrl = streamMedia(name);
        CommandPacket streamMrl = new CommandPacket(CommandType.STREAM_FILM.getId() + mrl);
        streamMrl.sendPacket(out);
    }


}
