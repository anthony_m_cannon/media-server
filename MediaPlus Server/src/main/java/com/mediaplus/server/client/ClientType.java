package com.mediaplus.server.client;

/**
 * Created by Deividas on 06/02/2015.
 */
public enum ClientType {

    UNKNOWN(0x0),
    DESKTOP(0x1),
    ANDROID(0x2);

    private int id;

    ClientType(int id){
        this.id = id;
    }

}
