package com.mediaplus.server.client;

import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Deividas on 02/03/2015.
 */
public abstract class ClientHandler {

    protected Client client;
    protected OutputStream out;
    protected InputStream in;

    public ClientHandler(Client client){
        this.client = client;
        this.out = client.getOutputStream();
        this.in = client.getInputStream();
    }

    public abstract boolean handleClient();
}
