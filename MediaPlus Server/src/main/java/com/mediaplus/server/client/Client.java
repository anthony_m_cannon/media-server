package com.mediaplus.server.client;

import com.mediaplus.server.io.MediaStream;
import com.mediaplus.server.io.packet.HandshakePacket;
import com.mediaplus.server.io.packet.Packet;
import com.mediaplus.server.util.Util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by Deividas on 30/01/2015.
 */
public class Client implements Runnable {

    private int desktopPin;
    private Socket socket;
    private int id;
    private InputStream in;
    private OutputStream out;
    private boolean connected;

    private ClientType clientType;

    private Thread clientThread;
    private MediaStream mediaStream;
    private boolean streaming;

    private ClientHandler clientHandler;

    public Client(Socket socket, int id){
        this.socket = socket;
        this.id = id;
        this.streaming = false;
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();

            //handshake

            HandshakePacket packet = (HandshakePacket) Packet.readPacket(in, this).get(0);
            clientType = packet.getClientType();

            if(clientType == ClientType.DESKTOP){
                clientHandler = new DesktopClient(this);
            }else if(clientType == ClientType.ANDROID){
                clientHandler = new AndroidClient(this);
            }

            System.out.println(id + " connected using " + clientType);
            connected = true;

            clientThread = new Thread(this, "Client " + id + " thread");
            clientThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while(connected){

            if(clientType == ClientType.UNKNOWN){
                System.out.println("Disconnecting " + id + ". Unknown client type.");
                disconnect();
            }else{
                clientHandler.handleClient();
            }
        }
    }

    public void disconnect(){
        try {
            in.close();
            out.close();
            socket.close();
            connected = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public int getId() {
        return id;
    }

    public OutputStream getOutputStream() {
        return out;
    }

    public InputStream getInputStream() {
        return in;
    }

    public ClientHandler getHandler() {
        return clientHandler;
    }
}
