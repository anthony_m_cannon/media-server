package com.mediaplus.server;

import com.mediaplus.server.client.Client;
import com.mediaplus.server.util.Constants;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

/**
 * Created by Deividas on 30/01/2015.
 */
public class Server{

    private ServerSocket serverSocket;
    private boolean running;
    private ArrayList<Client> clients;

    public Server(int port){
        System.setProperty("Dvlcj.log", "DEBUG");
        BaseObject.server = this;

        try {
            clients = new ArrayList<Client>();
            serverSocket = new ServerSocket(port);

            running = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(){
        System.out.println("Server started on " + Server.getServerIP() + ":" + Constants.SERVER_PORT);
        while(running){
            try {
                Socket clientSocket = serverSocket.accept();

                String clientIP = clientSocket.getInetAddress().getHostAddress();
                int clientPort = clientSocket.getPort();
                System.out.println("Connection accepted from " + clientIP + ":" + clientPort);

                Client client = new Client(clientSocket, clients.size());
                clients.add(client);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getServerIP(){
        try {
            return Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Client> getClients(){
        return clients;
    }

}
