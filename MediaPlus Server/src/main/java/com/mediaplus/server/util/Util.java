package com.mediaplus.server.util;

import java.util.Random;

/**
 * Created by Deividas on 27/02/2015.
 */
public class Util {

    private static Random rand = new Random();

    public static int generateNumber(int min, int max){
        int number = rand.nextInt(max - min) + min;

        return number;
    }

}
