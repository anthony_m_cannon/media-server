package com.mediaplus.server.util;

/**
 * Created by Deividas on 02/03/2015.
 */
public interface Constants {

    public static int SERVER_PORT = 4403;
    public static int STREAM_PORT = 8081;

    public static String MOVIE_ROOT_DIR = "D:\\Media";
    public static char END_OF_INPUT = (char) 65535;
}
