import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class RemoteTest extends JDialog {
    private JPanel contentPane;
    private JTextField pinField;
    private JButton connectButton;
    private InputStream in;
    private OutputStream out;

    public RemoteTest() {
        setContentPane(contentPane);
        setModal(true);
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(connect(getPin())){
                    System.out.println("Connected successfully");
                }else{
                    System.out.println("Could not perform handshake.");
                }
            }
        });
    }

    public static void main(String[] args) {
        RemoteTest dialog = new RemoteTest();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public int getPin(){
        return Integer.parseInt(pinField.getText());
    }

    public boolean connect(int pin){
        try {
            Socket sock = new Socket("127.0.0.1", 4403);
            in = sock.getInputStream();
            out = sock.getOutputStream();

            String handshakeString = "0102";
            writePacket(handshakeString);

            Thread.sleep(100);

            String pinString = "0218" + pin;
            writePacket(pinString);

            String response = readPacket(in);
            if(response.equals("02180")){
                return false;
            }else{
                return true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public void writePacket(String str){
        try {
            out.write(str.getBytes());
            out.flush();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public String readPacket(InputStream in) {
        try {
            //allocate byte buffer
            byte[] packetBytes = new byte[1024];
            while(in.read(packetBytes) != -1){
                String packetStr = new String(packetBytes, "UTF-8");
                return packetStr.trim();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }
}
