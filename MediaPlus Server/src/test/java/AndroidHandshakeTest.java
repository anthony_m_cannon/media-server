import com.mediaplus.server.client.Client;
import com.mediaplus.server.io.packet.CommandPacket;
import com.mediaplus.server.io.packet.HandshakePacket;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by Deividas on 06/03/2015.
 */
public class AndroidHandshakeTest {

    public static void main(String[] args){
        new AndroidHandshakeTest();
    }

    private int pin = 8160;

    public AndroidHandshakeTest(){

        try {
            Socket sock = new Socket("127.0.0.1", 4403);
            InputStream in = sock.getInputStream();
            OutputStream out = sock.getOutputStream();

            String handshakeString = "0102";
            out.write(handshakeString.getBytes());
            out.flush();

            Thread.sleep(100);

            String pinString = "0218" + pin;
            out.write(pinString.getBytes());
            out.flush();

            System.out.println("RESPONSE: " + readPacket(in));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String readPacket(InputStream in) {
        try {
            //allocate byte buffer
            byte[] packetBytes = new byte[1024];
            while(in.read(packetBytes) != -1){
                String packetStr = new String(packetBytes, "UTF-8");
                return packetStr.trim();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

}
