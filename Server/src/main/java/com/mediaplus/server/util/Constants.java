package com.mediaplus.server.util;

/**
 * Created by Deividas on 02/03/2015.
 */
public interface Constants {

    public static int SERVER_PORT = 4403;
    public static int STREAM_PORT = 8081;

    public static String MOVIE_ROOT_DIR = System.getProperty("user.home") + "\\Media+";
    public static final String END_OF_INPUT = "0END0";
}
