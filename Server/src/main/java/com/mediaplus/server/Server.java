package com.mediaplus.server;

import com.mediaplus.server.client.Client;
import com.mediaplus.server.util.Constants;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

/**
 * Created by Deividas on 30/01/2015.
 */
public class Server{

    private ServerSocket serverSocket;
    private boolean running;
    private ArrayList<Client> clients;

    public Server(int port){
        BaseObject.server = this;

        File homeCheck = new File(Constants.MOVIE_ROOT_DIR);
        if(!homeCheck.exists()){
            homeCheck.mkdir();
            File movies = new File(Constants.MOVIE_ROOT_DIR + "\\films");
            movies.mkdir();
            File music = new File(Constants.MOVIE_ROOT_DIR  + "\\music");
            music.mkdir();
            File pictures = new File(Constants.MOVIE_ROOT_DIR + "\\pictures");
            pictures.mkdir();
            System.out.println("-------------------------------------");
            System.out.println("We were unable to find your Media+ media directory");
            System.out.println("Created a new one at : " + Constants.MOVIE_ROOT_DIR);
            System.out.println("-------------------------------------");
        }

        try {
            clients = new ArrayList<Client>();
            serverSocket = new ServerSocket(port);

            running = true;
        } catch (IOException e) {
            if(e.getMessage().equals("Address already in use: JVM_Bind")){
                System.out.println("Port " + port + " is currently in use.");
                System.out.println("Please make sure no other applications are using this port.");
                System.exit(1);
            }else {
                e.printStackTrace();
            }
        }
    }

    public void start(){
        System.out.println("Server started on " + Server.getServerIP() + ":" + Constants.SERVER_PORT);
        while(running){
            try {
                Socket clientSocket = serverSocket.accept();

                String clientIP = clientSocket.getInetAddress().getHostAddress();
                int clientPort = clientSocket.getPort();
                System.out.println("Connection accepted from " + clientIP + ":" + clientPort);

                Client client = new Client(clientSocket, clients.size());
                clients.add(client);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getServerIP(){
        try {
            return Inet4Address.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<Client> getClients(){
        return clients;
    }

}
