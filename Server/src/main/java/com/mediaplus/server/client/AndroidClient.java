package com.mediaplus.server.client;

import com.mediaplus.server.BaseObject;
import com.mediaplus.server.io.FileIO;
import com.mediaplus.server.io.imdb.MovieInfo;
import com.mediaplus.server.io.packet.CommandPacket;
import com.mediaplus.server.io.packet.CommandProperty;
import com.mediaplus.server.io.packet.CommandType;
import com.mediaplus.server.io.packet.Packet;
import org.apache.commons.codec.binary.Base64;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Deividas on 02/03/2015.
 */
public class AndroidClient extends ClientHandler implements RemoteController {

    private int connectionPin = -1;
    private DesktopClient controlClient;
    private int pin;

    public AndroidClient(Client client) {
        super(client);

    }

    @Override
    public boolean handleClient(ArrayList<Packet> packets) {

        for (int i = 0; i < packets.size(); i++) {
            Packet packet = packets.get(i);
            if(packet instanceof CommandPacket){
                CommandPacket commandPacket = (CommandPacket) packet;
                handleCommand(commandPacket);
            }
        }

        return false;
    }

    @Override
    public void disconnect() {
        for(int i = 0; i < BaseObject.server.getClients().size();i++){
            if(BaseObject.server.getClients().get(i).getHandler() instanceof DesktopClient){
                DesktopClient dc = (DesktopClient) BaseObject.server.getClients().get(i).getHandler();
                dc.remoteControllers.remove(this);
            }
        }
    }

    private void handleCommand(CommandPacket commandPacket) {
        try {
            int commandId = commandPacket.getCommandId();
            CommandType type = CommandType.getCommandType(commandId);
            HeadlessMediaPlayer mediaPlayer = null;
            if (!(type == CommandType.BE_REMOTE || type == CommandType.GET_FILMS || type == CommandType.INFO
                    || type == CommandType.GET_PROPERTY || type == CommandType.GET_IMAGES
                    || type == CommandType.GET_MUSIC)) {
                if (controlClient == null) return;
                mediaPlayer = controlClient.getMediaPlayer();
            }
            switch (type) {
                case PLAY_PAUSE: //play/pause
                    controlClient.playPause();
                    break;
                case STOP://stop
                    controlClient.stop();
                    break;
                case LEFT://left
                    controlClient.left();
                    break;
                case RIGHT://right
                    controlClient.right();
                    break;
                case UP://up
                    controlClient.up();
                    break;
                case DOWN://down
                    controlClient.down();
                    break;
                case ENTER://enter
                    controlClient.enter();
                    break;
                case BACK://back
                    controlClient.back();
                    break;
                case INFO:
                    sendInfo(commandPacket.getCommandData());
                    break;
                case REPEAT://repeat
                    controlClient.repeat();
                    break;
                case SHUFFLE://shuffle
                    controlClient.shuffle();
                    break;
                case SET_PROPERTY://volume level (+ level)
                    int propertyId = Integer.parseInt(commandPacket.getCommandData().substring(0, 2));
                    String propertyData = commandPacket.getCommandData().substring(2);
                    CommandProperty property = CommandProperty.getProperty(propertyId);

                    switch (property) {
                        case ERROR:

                            break;
                        case VOLUME:
                            controlClient.volume(Integer.parseInt(propertyData));
                            break;
                        case CURRENT_PLAY_TIME:
                            controlClient.playTime(Long.parseLong(propertyData));
                            controlClient.setTime(Long.parseLong(propertyData));
                            break;
                    }


                    break;
                case GET_PROPERTY://film position (+ position)
                    int propId = Integer.parseInt(commandPacket.getCommandData().substring(0, 2));
                    String data = commandPacket.getCommandData().substring(2);
                    CommandProperty prop = CommandProperty.getProperty(propId);
                    switch(prop){
                        case ERROR:
                            break;
                        case VOLUME:
                            controlClient.getDesktpVolume();
                            break;
                        case PLAY_TIME:
                            sendCommand(CommandType.GET_PROPERTY.getId(), "0" + 2 + controlClient.getMediaPlayer().getLength());
                            break;
                        case VIDEO_CHANNEL:
                            break;
                        case AUDIO_CHANNEL:
                            break;
                        case SUBTITLE_CHANNEL:
                            break;
                        case PAUSE_STATE:
                            break;
                        case FILM_POSTER:
                            MovieInfo info = new MovieInfo();
                            HashMap<String, String> execute = info.getFilm(FileIO.stripExtension(data));
                            sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.FILM_POSTER.getId() + "" + data + "¦" + execute.get("poster"));
                            break;
                        case NO_FILMS:
                            sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.NO_FILMS.getId() + "" + FileIO.getNoFilms());
                            break;
                    }
                    break;
                case MUTE://mute
                    controlClient.mute();
                    break;
                case POWER://power
                    controlClient.power();
                    break;
                case STREAM_FILM://stream film (+ name)
                    String name = commandPacket.getCommandData();
                    controlClient.stream(name, 0, MediaType.FILM);

                    break;
                case GET_FILMS:
                    File[] films = FileIO.getFilms();
                    for(int j = 0;j < films.length;j++){
                        sendCommand(CommandType.GET_FILMS.getId(), films[j].getName());
                    }
                    break;
                case GET_IMAGES:
                    System.out.println("GET IMAGES");
                    File[] images = FileIO.getImages();
                    for (File image : images) {
                        try {
                            ByteArrayOutputStream imageOut = new ByteArrayOutputStream();
                            BufferedImage img = ImageIO.read(image);
                            String imgType = image.getName().substring(image.getName().lastIndexOf(".") + 1);
                            ImageIO.write(img, imgType, imageOut);

                            String encodedImage = Base64.encodeBase64String(imageOut.toByteArray());

                            sendCommand(CommandType.GET_IMAGES.getId(), image.getName() + "¦" + encodedImage);

                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    break;
                case GET_MUSIC:
                    File[] music = FileIO.getMusic();
                    for(int j = 0;j < music.length;j++){
                        sendCommand(CommandType.GET_MUSIC.getId(), music[j].getName());
                    }
                    break;
                case PLAY_SONG:
                    String songName = commandPacket.getCommandData();
                    controlClient.stream(songName, 0, MediaType.AUDIO);
                    //controlClient.sendCommand(CommandType.PLAY_SONG.getId(), commandPacket.getCommandData());
                    break;
                case SHOW_IMAGE:
                    //controlClient.sendCommand(CommandType.SHOW_IMAGE.getId(), commandPacket.getCommandData());
//                    String imageName = commandPacket.getCommandData();
//                    controlClient.stream(imageName, MediaType.IMAGE);
                    break;
                case BE_REMOTE://be remote
                    int pin = Integer.parseInt(commandPacket.getCommandData());
                    if (setControlClient(pin)) {
                        CommandPacket pinResponse = new CommandPacket(CommandType.BE_REMOTE.getId() + "1");
                        pinResponse.sendPacket(out, client, true);
                        sendPlayTime();
                    } else {
                        CommandPacket pinResponse = new CommandPacket(CommandType.BE_REMOTE.getId() + "0");
                        pinResponse.sendPacket(out, client, true);
                    }
                    break;
            }
        }catch(Exception e){
            e.printStackTrace();
            System.err.println("Could not process packet");
        }
    }



    public void sendPlayTime() {
        long playTime = controlClient.getMediaPlayer().getLength();
        if(playTime == -1){
            playTime = 0;
        }

        sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.PLAY_TIME.getId() + playTime);
    }

    public void sendCurrentPlayTime(long playTime) {
        if(playTime == -1){
            playTime = 0;
        }

        sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.CURRENT_PLAY_TIME.getId() + playTime);
    }

    public boolean setControlClient(int pin) {
        ArrayList<Client> clients = BaseObject.server.getClients();
        for(Client c : clients){
            if(c.getHandler() instanceof DesktopClient){
                DesktopClient d = (DesktopClient) c.getHandler();
                if(d.getPin() == pin){
                    controlClient = d;
                    connectionPin = pin;
                    d.remoteControllers.add(this);
                    return true;
                }
            }
        }
        return false;
    }

    public DesktopClient getControlClient() {
        return controlClient;
    }

    public void setControlClient(DesktopClient controlClient) {
        this.controlClient = controlClient;
    }

    private void sendFilms() {
        File[] films = FileIO.getFilms();
        for (int i = 0; i < films.length; i++) {
            CommandPacket filmCommand = new CommandPacket(CommandType.GET_FILMS.getId() + films[i].getName());
            filmCommand.sendPacket(out, client);
        }
    }

    private void sendInfo(String movie) {
        String movieName = FileIO.stripExtension(movie);
        MovieInfo info = new MovieInfo();
        HashMap<String, String> execute = info.getFilm(movieName);

        final String SEPERATOR = "¦";

        HashMap<String, String> data = new HashMap<String, String>();
        if(execute != null){
            data.putAll(execute);
        }
        if(execute == null){
            data.put("title", movieName);
            data.put("poster", "N/A");
            data.put("year", "0");
            data.put("runtime", "0 mins");
            data.put("genre", "N/A");
            data.put("imdbRating", "0.0");
            data.put("tomatoRating", "0.0");
            data.put("director", "N/A");
            data.put("actors", "N/A");
            data.put("plot", "N/A");
            data.put("rated", "N/A");
            data.put("country", "N/A");
        }else{
            if(execute.get("title").equals("null")){
                data.put("title", movie);
            }
            if(execute.get("poster") == null){
                data.put("poster", "N/A");
            }
            if(execute.get("year") == null){
                data.put("year", "0");
            }
            if(execute.get("runtime") == null){
                data.put("runtime", "0.0");
            }
            if(execute.get("genre") == null){
                data.put("genre", "N/A");
            }
            if(execute.get("imdbRating") == null){
                data.put("imdbRating", "0.0");
            }
            if(execute.get("tomatoRating") == null){
                data.put("tomatoRating", "0.0");
            }
            if(execute.get("director") == null){
                data.put("director", "N/A");
            }
            if(execute.get("actors") == null){
                data.put("actors", "N/A");
            }
            if(execute.get("plot") == null){
                data.put("plot", "N/A");
            }
            if(execute.get("rated") == null){
                data.put("rated", "N/A");
            }
            if(execute.get("country") == null){
                data.put("country", "N/A");
            }
        }

        String dataSend = "";
        dataSend += "title=" + data.get("title") + SEPERATOR;
        dataSend += "poster=" + data.get("poster") + SEPERATOR;
        dataSend += "year=" + data.get("year") + SEPERATOR;
        dataSend += "runtime=" + data.get("runtime") + SEPERATOR;
        dataSend += "genre="            +   data.get("genre") + SEPERATOR;
        dataSend += "imdbRating="       +   data.get("imdbRating") + SEPERATOR;
        dataSend += "tomatoRating="     +   data.get("tomatoRating") + SEPERATOR;
        dataSend += "director="         +   data.get("director") + SEPERATOR;
        dataSend += "actors="           +   data.get("actors") + SEPERATOR;
        dataSend += "plot="             +   data.get("plot") + SEPERATOR;
        dataSend += "rated="            +   data.get("rated") + SEPERATOR;
        dataSend += "country="          +   data.get("country") + SEPERATOR;
        dataSend += "fileName="         +   movie;

        sendCommand(CommandType.INFO.getId(), dataSend);
    }

    public void sendCommand(int commandId){
        sendCommand(commandId, "");
    }

    public void sendCommand(CommandType type){
        sendCommand(type.getId(), "");
    }


    public void sendCommand(int commandId, String commandData){
        String id = "";
        if(commandId < 10){
            id += 0;
        }
        id += commandId;
        CommandPacket commandPacket = new CommandPacket(id + commandData);
        commandPacket.sendPacket(out, client);
    }
    @Override
    public OutputStream getOutputStream() {
        return out;
    }

    public Client getClient(){
        return client;
    }

    public int getPin() {
        return pin;
    }

}
