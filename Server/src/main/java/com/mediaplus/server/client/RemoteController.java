package com.mediaplus.server.client;

import java.io.OutputStream;

/**
 * Created by Deividas on 02/03/2015.
 */
public interface RemoteController {


    OutputStream getOutputStream();

    Client getClient();
}
