package com.mediaplus.server.client;

import com.mediaplus.server.io.packet.Packet;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * Created by Deividas on 02/03/2015.
 */
public abstract class ClientHandler {

    protected Client client;
    protected OutputStream out;
    protected InputStream in;

    public ClientHandler(Client client){
        this.client = client;
        this.out = client.getOutputStream();
        this.in = client.getInputStream();
    }

    public abstract boolean handleClient(ArrayList<Packet> packets);

    public abstract void disconnect();
}
