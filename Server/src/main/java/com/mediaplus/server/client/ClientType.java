package com.mediaplus.server.client;

/**
 * Created by Deividas on 06/02/2015.
 */
public enum ClientType {

    UNKNOWN(),
    DESKTOP(),
    ANDROID();


    ClientType(){
    }

}
