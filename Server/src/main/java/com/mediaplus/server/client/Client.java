package com.mediaplus.server.client;

import com.mediaplus.server.BaseObject;
import com.mediaplus.server.io.packet.HandshakePacket;
import com.mediaplus.server.io.packet.Packet;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by Deividas on 30/01/2015.
 */
public class Client implements Runnable {

    private Socket socket;
    private int id;
    private InputStream in;
    private OutputStream out;
    private boolean connected;

    private ClientType clientType;

    private Thread clientThread;

    private ClientHandler clientHandler;
    private static int desktopConnections = 0;

    public Client(Socket socket, int id){
        this.socket = socket;
        this.id = id;
        try {
            in = socket.getInputStream();
            out = socket.getOutputStream();

            //handshake

            HandshakePacket packet = (HandshakePacket) Packet.readPacket(in, this).get(0);
            clientType = packet.getClientType();

            if(clientType == ClientType.DESKTOP){
                clientHandler = new DesktopClient(this, desktopConnections);
                desktopConnections++;
            }else if(clientType == ClientType.ANDROID){
                clientHandler = new AndroidClient(this);
            }

            switch (clientType){
                case UNKNOWN:
                    break;
                case DESKTOP:
                    System.out.println("Accepted desktop connection");
                    break;
                case ANDROID:
                    System.out.println("Accepted android connection");
                    break;
            }

            connected = true;

            clientThread = new Thread(this, "Client " + id + " thread");
            clientThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while(connected){

            if(clientType == ClientType.UNKNOWN){
                System.out.println("Disconnecting " + id + ". Unknown client type.");
                disconnect();
            }else{
                ArrayList<Packet> packets = Packet.readPacket(in, this);
                clientHandler.handleClient(packets);
            }

            try {
                Thread.sleep(50);
            } catch (InterruptedException ignored) {}

        }

    }

    public void disconnect(){
        try {

            clientThread.interrupt();
            connected = false;
            in.close();
            out.close();
            socket.close();

            clientHandler.disconnect();
            BaseObject.server.getClients().remove(this);
            Thread.currentThread().interrupt();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public OutputStream getOutputStream() {
        return out;
    }

    public InputStream getInputStream() {
        return in;
    }

    public ClientHandler getHandler() {
        return clientHandler;
    }

    public boolean isConnected() {
        return connected;
    }
}
