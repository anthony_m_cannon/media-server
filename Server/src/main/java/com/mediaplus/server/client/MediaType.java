package com.mediaplus.server.client;

/**
 * Created by Deividas on 25/03/2015.
 */
public enum MediaType {
    FILM, AUDIO, IMAGE;
}
