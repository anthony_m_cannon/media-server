package com.mediaplus.server.client;

import com.mediaplus.server.BaseObject;
import com.mediaplus.server.Server;
import com.mediaplus.server.io.FileIO;
import com.mediaplus.server.io.ImageFileFilter;
import com.mediaplus.server.io.imdb.MovieInfo;
import com.mediaplus.server.io.packet.*;
import com.mediaplus.server.util.Constants;
import com.mediaplus.server.util.Util;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import uk.co.caprica.vlcj.filter.AudioFileFilter;
import uk.co.caprica.vlcj.filter.VideoFileFilter;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;
import uk.co.caprica.vlcj.player.headless.HeadlessMediaPlayer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Deividas on 02/03/2015.
 */
public class DesktopClient extends ClientHandler implements Runnable{

    private final HeadlessMediaPlayer mediaPlayer;
    private Thread updateThread;
    private int pin;
    public ArrayList<RemoteController> remoteControllers;
    private int desktopId = -1;
    private String currentPath;
    private MediaType currentType;
    private boolean playing = false;
    public long timeTest = 0;

    public DesktopClient(Client client, int desktopId) {
        super(client);

        this.desktopId = desktopId;

        MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
        mediaPlayer = mediaPlayerFactory.newHeadlessMediaPlayer();


        pin = Util.generateNumber(1000, 9999);
        while(!isPinUnique(pin)){
            pin = Util.generateNumber(1000, 9999);
        }

//        pin = 123;

        HandshakePacket handshakePacket = new HandshakePacket(Packet.PacketType.HANDSHAKE.getId(), pin + "");
        handshakePacket.sendPacket(out, client, true);

        remoteControllers = new ArrayList<RemoteController>();

        updateThread = new Thread(this);
        updateThread.start();

    }

    @Override
    public boolean handleClient(ArrayList<Packet> packets) {

        for (Packet packet : packets) {
            if (packet instanceof CommandPacket) {
                CommandPacket commandPacket = (CommandPacket) packet;
                int id = commandPacket.getCommandId();
                CommandType command = CommandType.getCommandType(id);
                switch (command) {
                    case GET_FILMS:
                        sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.NO_FILMS.getId() + "" + getNoFilms());
                        sendCommand(CommandType.SET_PROPERTY.getId(), CommandProperty.NO_MUSIC.getId() + "" + getNoMusic());
                        sendCommand(CommandType.SET_PROPERTY.getId(), CommandProperty.NO_IMAGES.getId() + "" + getNoImages());

                        File[] films = getFilms();
                        for (File film : films) {
                            sendCommand(CommandType.GET_FILMS.getId(), film.getName());
                        }
                        break;
                    case GET_IMAGES:
                        File[] images = FileIO.getImages();
                        for (File image : images) {
                            try {
                                ByteArrayOutputStream imageOut = new ByteArrayOutputStream();
                                BufferedImage img = ImageIO.read(image);
                                String imgType = image.getName().substring(image.getName().lastIndexOf(".") + 1);
                                ImageIO.write(img, imgType, imageOut);
                                String encodedImage = Base64.encode(imageOut.toByteArray());

                                sendCommand(CommandType.GET_IMAGES.getId(), image.getName() + "¦" + encodedImage);

                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        break;
                    case GET_MUSIC:
                        File[] music = FileIO.getMusic();
                        for (File aMusic : music) {
                            sendCommand(CommandType.GET_MUSIC.getId(), aMusic.getName());
                        }
                        break;
                    case INFO:
                        sendInfo(commandPacket.getCommandData());
                        break;
                    case GET_PROPERTY:
                        int getProperty = Integer.parseInt(commandPacket.getCommandData().substring(0, 2));
                        CommandProperty commandProperty = CommandProperty.getProperty(getProperty);
                        String data = "";
                        if (commandPacket.getCommandData().length() > 2) {
                            data += commandPacket.getCommandData().substring(2);
                        }
                        String packetBuilder;
                        switch (commandProperty) {
                            case PLAY_TIME:
                                //System.out.println("PLAY TIME " + mediaPlayer.getTime());
                                break;
                            case VOLUME:

                                break;
                            case NO_FILMS:
                                sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.NO_FILMS.getId() + "" + getNoFilms());
                                break;
                            case NO_MUSIC:
                                sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.NO_MUSIC.getId() + "" + getNoMusic());
                                break;
                            case NO_IMAGES:
                                sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.NO_IMAGES.getId() + "" + getNoImages());
                                break;
                            case FILM_POSTER:
                                MovieInfo info = new MovieInfo();
                                HashMap<String, String> execute = info.getFilm(stripExtension(data));
                                String poster = (execute == null || execute.get("poster") == null ? "N/A" : execute.get("poster"));
                                sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.FILM_POSTER.getId() + "" + data + "¦" + poster);
                                break;
                        }
                        break;
                    case SET_PROPERTY:
                        int setProperty = Integer.parseInt(commandPacket.getCommandData().substring(0, 2));
                        String propertyData = commandPacket.getCommandData().substring(2);
                        CommandProperty commandProperty1 = CommandProperty.getProperty(setProperty);
                        switch (commandProperty1) {
                            case VOLUME:
                                packetBuilder = "";
                                packetBuilder += CommandType.SET_PROPERTY.getId();
                                packetBuilder += "0" + CommandProperty.VOLUME.getId();
                                packetBuilder += propertyData;
                                try {
                                    for (RemoteController remoteController1 : remoteControllers) {
                                        CommandPacket volumeUpdate = new CommandPacket(packetBuilder);
                                        volumeUpdate.sendPacket(remoteController1.getOutputStream(), remoteController1.getClient());
                                    }
                                }catch(Exception ignored){}//concurrentmodification probably.
                                sendCommand(CommandType.GET_PROPERTY.getId(), CommandProperty.PAUSE_STATE.getId() + "");
                                break;
                            case PAUSE_STATE:
                                int desktopPauseState = Integer.parseInt(propertyData);
                                packetBuilder = "";
                                packetBuilder += CommandType.SET_PROPERTY.getId();
                                packetBuilder += "0" + CommandProperty.PAUSE_STATE.getId();
                                packetBuilder += desktopPauseState;
                                for (RemoteController remoteController : remoteControllers) {
                                    CommandPacket pauseUpdate = new CommandPacket(packetBuilder);
                                    pauseUpdate.sendPacket(remoteController.getOutputStream(), remoteController.getClient());
                                }

                                break;
                            case CURRENT_PLAY_TIME:
                                long totalPlayTime = Long.parseLong(propertyData);
                                packetBuilder = "";
                                packetBuilder += CommandType.SET_PROPERTY.getId();
                                packetBuilder += "0" + CommandProperty.CURRENT_PLAY_TIME.getId();
                                packetBuilder += totalPlayTime;
                                timeTest = totalPlayTime;
                                for (RemoteController remoteController : remoteControllers) {
                                    CommandPacket playTimeUpdate = new CommandPacket(packetBuilder);
                                    playTimeUpdate.sendPacket(remoteController.getOutputStream(), remoteController.getClient());
                                }

                                break;
                        }

                        break;
                    case STREAM_FILM://stream film (+ name)
                        String name = commandPacket.getCommandData();
                        if(name.equals(currentPath)) {
                            stream(currentPath, timeTest, MediaType.FILM);
                        }else{
                            stream(name, 0, MediaType.FILM);
                        }
                        sendCommand(CommandType.GET_PROPERTY.getId(), CommandProperty.VOLUME.getId() + "");
                        try {
                            for (RemoteController remoteController : remoteControllers) {
                                if (remoteController instanceof AndroidClient) {
                                    AndroidClient ac = (AndroidClient) remoteController;
//                                    ac.sendCurrentPlayTime(timeTest);
                                    ac.sendPlayTime();
                                }
                            }
                        }catch(Exception ignored){}
                        break;
                    case PLAY_SONG:
                        String songName = commandPacket.getCommandData();
                        stream(songName, 0, MediaType.AUDIO);
                        try {
                            for (RemoteController remoteController : remoteControllers) {
                                if (remoteController instanceof AndroidClient) {
                                    AndroidClient ac = (AndroidClient) remoteController;
//                                    ac.sendCurrentPlayTime(timeTest);
                                    ac.sendPlayTime();
                                }
                            }
                        }catch(Exception ignored){}
                        break;
                    default:
                        break;
                }
            }
        }

        return true;
    }

    private void sendInfo(String movie) {
        String movieName = stripExtension(movie);
        MovieInfo info = new MovieInfo();
        HashMap<String, String> execute = info.getFilm(movieName);

        final String SEPERATOR = "¦";

        HashMap<String, String> data = new HashMap<String, String>();

        if(execute == null){
            data.put("title", movie);
            data.put("poster", "N/A");
            data.put("year", "0");
            data.put("runtime", "0 mins");
            data.put("genre", "N/A");
            data.put("imdbRating", "0.0");
            data.put("tomatoRating", "0.0");
            data.put("director", "N/A");
            data.put("actors", "N/A");
            data.put("plot", "N/A");
            data.put("rated", "N/A");
            data.put("country", "N/A");
        }else{
            data.putAll(execute);
            if(execute.get("title").equals("null")){
                data.put("title", movie);
            }
            if(execute.get("poster") == null){
                data.put("poster", "N/A");
            }
            if(execute.get("year") == null){
                data.put("year", "0");
            }
            if(execute.get("runtime") == null){
                data.put("runtime", "0.0");
            }
            if(execute.get("genre") == null){
                data.put("genre", "N/A");
            }
            if(execute.get("imdbRating") == null){
                data.put("imdbRating", "0.0");
            }
            if(execute.get("tomatoRating") == null){
                data.put("tomatoRating", "0.0");
            }
            if(execute.get("director") == null){
                data.put("director", "N/A");
            }
            if(execute.get("actors") == null){
                data.put("actors", "N/A");
            }
            if(execute.get("plot") == null){
                data.put("plot", "N/A");
            }
            if(execute.get("rated") == null){
                data.put("rated", "N/A");
            }
            if(execute.get("country") == null){
                data.put("country", "N/A");
            }
        }

        String dataSend = "";
        dataSend += "title=" + data.get("title") + SEPERATOR;
        dataSend += "poster=" + data.get("poster") + SEPERATOR;
        dataSend += "year=" + data.get("year") + SEPERATOR;
        dataSend += "runtime=" + data.get("runtime") + SEPERATOR;
        dataSend += "genre="            +   data.get("genre") + SEPERATOR;
        dataSend += "imdbRating="       +   data.get("imdbRating") + SEPERATOR;
        dataSend += "tomatoRating="     +   data.get("tomatoRating") + SEPERATOR;
        dataSend += "director="         +   data.get("director") + SEPERATOR;
        dataSend += "actors="           +   data.get("actors") + SEPERATOR;
        dataSend += "plot="             +   data.get("plot") + SEPERATOR;
        dataSend += "rated="            +   data.get("rated") + SEPERATOR;
        dataSend += "country="          +   data.get("country") + SEPERATOR;
        dataSend += "fileName="         +   movie;

        sendCommand(CommandType.INFO.getId(), dataSend);
    }

    @Override
    public void disconnect() {
        updateThread.interrupt();
        for(RemoteController remoteController : remoteControllers){
            if(remoteController instanceof AndroidClient){
                AndroidClient ac = (AndroidClient) remoteController;
                ac.sendCommand(CommandType.DISCONNECTED_DESKTOP);
            }
        }
    }

    @SuppressWarnings("unused")
    private boolean isPinUnique(int pin){
        ArrayList<Client> clients = BaseObject.server.getClients();
        for (Client c : clients) {
            if(c.getHandler() instanceof DesktopClient){
                DesktopClient handler = (DesktopClient) c.getHandler();
                if(handler.getPin() == pin){
                    return false;
                }
            }
        }

        return true;
    }

    public int getPin() {
        return pin;
    }

    private String streamMedia(String path, long time, MediaType type){

        currentPath = path;
        currentType = type;

        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }

        String streamMrl = Server.getServerIP() + ":" + (Constants.STREAM_PORT + desktopId) + "/" + desktopId;

        System.out.println("-------------------------");
        System.out.println("Starting new stream");
        System.out.println("-------------------------");

        String options = ":sout=#duplicate{dst=std{access=http,mux=ts,dst=";
        options += streamMrl;
        options += "}}";

        String subDirectory;
        if(type == MediaType.AUDIO) {
            subDirectory = "music";
        }else if(type == MediaType.IMAGE){
            subDirectory = "pictures";
        }else{
            subDirectory = "films";
        }

        String streamPath = Constants.MOVIE_ROOT_DIR + "\\" + subDirectory + "\\" + path;
        mediaPlayer.playMedia(streamPath, options, ":sout-all", ":start-time=" + (time == 0 ? 0 : time / 1000));

        try {
            for (RemoteController remoteController : remoteControllers) {
                if (remoteController instanceof AndroidClient) {
                    AndroidClient ac = (AndroidClient) remoteController;
//                                    ac.sendCurrentPlayTime(timeTest);
                    ac.sendPlayTime();
                }
            }
        }catch(Exception ignored){}

        return streamMrl;
    }

    public HeadlessMediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void sendCommand(CommandType type){
        sendCommand(type.getId(), "");
    }


    public void sendCommand(int commandId, String commandData){
        String id = "";
        if(commandId < 10){
            id += 0;
        }
        id += commandId;
        CommandPacket commandPacket = new CommandPacket(id + commandData);
        commandPacket.sendPacket(out, client);
    }

    public void playPause(){
        playing = !playing;
        /*
        if(mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }else{
            mediaPlayer.play();
        }*/
        sendCommand(CommandType.PLAY_PAUSE);
    }

    public void stop() {
        mediaPlayer.stop();
        sendCommand(CommandType.STOP);
    }

    public void left() {

    }

    public void right() {

    }

    public void up() {
    }

    public void down() {
    }

    public void enter() {
    }

    public void back() {
    }

    public void repeat() {
        if(mediaPlayer.getRepeat()){
            mediaPlayer.setRepeat(false);
        }else {
            mediaPlayer.setRepeat(true);
        }
    }

    public void shuffle() {
    }

    public void volume(int volume) {
        //1301[volume]
        sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.VOLUME.getId() + volume + "");
    }

    public void mute(){
        sendCommand(CommandType.MUTE);
    }

    public void power() {

    }

    public void stream(String name, long time ,MediaType type) {
        String mrl = streamMedia(name, time, type);
        CommandPacket streamMrl = new CommandPacket(CommandType.STREAM_FILM.getId() + mrl + "¦" + time);
        streamMrl.sendPacket(out, client);

        for (RemoteController remoteController : remoteControllers) {
            AndroidClient androidClient = (AndroidClient) remoteController;
            androidClient.sendPlayTime();
            androidClient.sendCurrentPlayTime(timeTest);
        }

    }


    @Override
    public void run() {
        long lastTime = System.currentTimeMillis();
        while(client.isConnected()){
            long now = System.currentTimeMillis();
            if(now - lastTime >= 1000){
                updateClient();
                lastTime = now;
            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException ignored) {}
        }
    }

    private void updateClient() {
        sendCommand(CommandType.GET_PROPERTY.getId(), CommandProperty.VOLUME.getId() + "");
        sendCommand(CommandType.GET_PROPERTY.getId(), CommandProperty.CURRENT_PLAY_TIME.getId() + "");

        try {
            for (RemoteController remoteController : remoteControllers) {
                if (remoteController instanceof AndroidClient) {
                    AndroidClient ac = (AndroidClient) remoteController;
                    ac.sendPlayTime();
                }
            }
        }catch(Exception ignored){}
    }

    public void getDesktpVolume() {
        String packet = "";
        packet += CommandType.GET_PROPERTY.getId();
        packet += CommandProperty.VOLUME.getId();
        CommandPacket getVolume = new CommandPacket(packet);
        getVolume.sendPacket(out, client);
    }

    private File[] getFilms(){
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\films");

        FileFilter filter = new VideoFileFilter();
        File[] files = folder.listFiles(filter);

        MovieInfo info = new MovieInfo();
        for (File file : files) {
            String fileName = stripExtension(file.getName());
            info.getFilm(fileName);
        }

        return files;
    }

    private int getNoFilms(){
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\films");
        FileFilter filter = new VideoFileFilter();
        File[] files = folder.listFiles(filter);
        return files.length;
    }

    public String stripExtension(String fileName){
        int index = fileName.lastIndexOf(".");

        if(index > 0){
            fileName = fileName.substring(0, index);
        }

        return fileName;
    }

    public int getNoImages() {
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\pictures");
        FileFilter filter = new ImageFileFilter();
        File[] files = folder.listFiles(filter);
        return files.length;
    }

    public int getNoMusic() {
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\music");
        FileFilter filter = new AudioFileFilter();
        File[] files = folder.listFiles(filter);
        return files.length;
    }

    public void playTime(long l) {
        String mrl = streamMedia(currentPath, l, currentType);

        sendCommand(CommandType.STREAM_FILM.getId(), mrl + "¦" + l);

//        sendCommand(CommandType.SET_PROPERTY.getId(), "0" + CommandProperty.CURRENT_PLAY_TIME.getId() + l);
    }

    public void setTime(long time){
        this.timeTest = time;
    }
}
