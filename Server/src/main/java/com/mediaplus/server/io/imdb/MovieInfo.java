package com.mediaplus.server.io.imdb;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Scanner;

public class MovieInfo {
	private MDLocal local;
	private MDOnline online;
	
	public MovieInfo() {
		local = new MDLocal();
		online = new MDOnline();
	}

	public HashMap<String, String> getFilm(String name) {
		local.newSearch(name);
		
		if (!local.exists()) {
			online.newSearch(name);
			HashMap<String, String> film = online.execute();
			
			if (film == null || film.size() == 0) {
				return null;
			}
			
			local.saveFile(name, hashMapToArrayList(film));
		}
		
		return local.execute();
	}

    @SuppressWarnings("unused")
    public HashMap<String, String> checkFilm(HashMap<String, String> execute, String fileName) {
        HashMap<String, String> data = new HashMap<String, String>(execute);
        if(execute == null){
            data.put("title", fileName);
            data.put("poster", "N/A");
            data.put("year", "0");
            data.put("runtime", "0 mins");
            data.put("genre", "N/A");
            data.put("imdbRating", "0.0");
            data.put("tomatoRating", "0.0");
            data.put("director", "N/A");
            data.put("actors", "N/A");
            data.put("plot", "N/A");
            data.put("rated", "N/A");
            data.put("country", "N/A");
        }else{
            if(execute.get("title").equals("null")){
                data.put("title", fileName);
            }
            if(execute.get("poster") == null){
                data.put("poster", "N/A");
            }
            if(execute.get("year") == null){
                data.put("year", "0");
            }
            if(execute.get("runtime") == null){
                data.put("runtime", "0.0");
            }
            if(execute.get("genre") == null){
                data.put("genre", "N/A");
            }
            if(execute.get("imdbRating") == null){
                data.put("imdbRating", "0.0");
            }
            if(execute.get("tomatoRating") == null){
                data.put("tomatoRating", "0.0");
            }
            if(execute.get("director") == null){
                data.put("director", "N/A");
            }
            if(execute.get("actors") == null){
                data.put("actors", "N/A");
            }
            if(execute.get("plot") == null){
                data.put("plot", "N/A");
            }
            if(execute.get("rated") == null){
                data.put("rated", "N/A");
            }
            if(execute.get("country") == null){
                data.put("country", "N/A");
            }
        }
        local.saveFile(fileName.substring(0, fileName.lastIndexOf(".")), hashMapToArrayList(data));

        return local.execute();
    }

    @SuppressWarnings("unused")
    public HashMap<String, HashMap<String, String>> getAllFilms() {
		
		return local.getAllFilms();
	}
	
	// Private
	private String[] hashMapToArrayList(HashMap<String, String> hMap) {
		String[] array = new String[hMap.size()];
		int i = 0;
		
		Iterator<Entry<String, String>> it = hMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Entry<String, String> pair = it.next();
	        array[i] = pair.getKey() + "=" + pair.getValue();
	        
	        it.remove(); // avoids a ConcurrentModificationException
	        i++;
	    }
	    
	    return array;
	}
}
