package com.mediaplus.server.io.imdb;

import com.mediaplus.server.io.FileIO;

import java.util.HashMap;

public class MDLocal {
	private final FileIO fileIo;
	private String fileName;
	private String folderName, folderPath;
	
	public MDLocal() {
		fileIo = new FileIO();
		folderName = "films";
		folderPath = folderName + "/";
		
		if (!directoryExists()) {
			createDirectory();
		}
	}
	
	public void newSearch(String fileName) {
		this.fileName = fileName;
	}
	
	public boolean exists() {
        if (fileName != null) {
			if (fileIo.fileExists(folderPath + fileName)) {
				return true;
			}
		}
		return false;
	}
	
	public HashMap<String, String> execute() {
		if (fileName != null) {
			if (fileIo.fileExists(folderPath + fileName)) {
				String[] file = fileIo.readFile(folderPath + fileName);
				fileName = null; // restart fileName
				return covertToHashMap(file);
			}
		}
		return null;
	}
	
	public boolean saveFile(String name, String[] lines) {
		boolean created = fileIo.createNewFile(folderPath + name);

        return created && fileIo.writeToFile(folderPath + name, false, lines);
    }

	public HashMap<String, HashMap<String, String>> getAllFilms() {
		String[] files = fileIo.listFilesInFolder(folderPath);
		HashMap<String, HashMap<String, String>> films = new HashMap<String, HashMap<String, String>>();
		HashMap<String, String> film;
		
		for (String file : files) {

            String[] fileContents = fileIo.readFile(folderPath + file);
			film = covertToHashMap(fileContents);
			
			films.put(film.get("title"), film);
		}
		
		return films;
	}
	
	// Private
	private HashMap<String, String> covertToHashMap(String[] sArray) {
		HashMap<String, String> hMap = new HashMap<String, String>();
		
		for (String string : sArray) {
			
			String[] s = string.split("=");
			hMap.put(s[0].trim(), s[1].trim());
		}
		
		return hMap;
	}
	
	private boolean directoryExists() {
        return fileIo.folderExists(folderName);
    }
	
	private boolean createDirectory() {
		return fileIo.createNewFolder(folderName);
	}
}
