package com.mediaplus.server.io.imdb;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class MDOnline {
	private String searchQuery;
	private HttpURLConnection connection;

	public MDOnline() {
		
	}
	
	public void newSearch(String name) {
		searchQuery = "t=" + name.replace(" ", "%20");
	}

	public HashMap<String, String> execute() {
		if (searchQuery != null) {
			HashMap<String, String> hMap = sendRequest();
			
			searchQuery = null; // restart fileName
			return hMap;
		}
		
		return null;
	}
	
	// Private
	private HashMap<String, String> sendRequest() {
		try {
			URL url = new URL("http://www.omdbapi.com/?" + searchQuery
					+ "&tomatoes=true" + "&r=xml");

			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");

			return getReply();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private HashMap<String, String> getReply() {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			String data = "";
			String line;

			while ((line = reader.readLine()) != null) {
				data += line.trim();
			}
			
			return formatReply(data);
		} catch (IOException e) {
			System.err.println("Connection timed out: Couldn't recieve a response.");
		}

		return null;
	}

	private HashMap<String, String> formatReply(String reply) {
		try {
			HashMap<String, String> map = new HashMap<String, String>();

			DocumentBuilder db = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(reply));

			Document doc = db.parse(is);
			NodeList rootNode = doc.getElementsByTagName("root");
			for (int i=0; i<rootNode.getLength(); i++) {
				NodeList movieNodes = rootNode.item(i).getChildNodes();
				for (int j=0; j<movieNodes.getLength(); j++) {
					NamedNodeMap movieAttributes = movieNodes.item(j)
							.getAttributes();
					for (int k=0; k<movieAttributes.getLength(); k++) {
						Node attribute = movieAttributes.item(k);
						
						// Testing

						map.put(attribute.getNodeName(),
								attribute.getNodeValue());

					}
				}
			}

			return map;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}

		return null;
	}
}
