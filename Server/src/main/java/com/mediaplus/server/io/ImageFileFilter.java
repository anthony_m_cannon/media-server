package com.mediaplus.server.io;

import java.io.File;
import java.io.FileFilter;

/**
 * Created by Deividas on 24/03/2015.
 */
public class ImageFileFilter implements FileFilter {

    private final String[] SUPPORTED_IMAGES =  {"png"};

    @Override
    public boolean accept(File pathname) {
        String fileName = pathname.getName();
        long length = pathname.getName().length() + pathname.length() + 5;
        if(length > Character.MAX_VALUE){
            System.out.println("Cannot send " + fileName + ". The image is too big!");
            return false;
        }
        if(fileName.lastIndexOf(".") == -1) return false;
        String ext = fileName.substring(fileName.lastIndexOf(".")+1);
        for (String supExt : SUPPORTED_IMAGES) {
            if(supExt.equalsIgnoreCase(ext)){
                return true;
            }
        }

        return false;
    }
}
