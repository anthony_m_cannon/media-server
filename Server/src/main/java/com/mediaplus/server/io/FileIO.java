package com.mediaplus.server.io;

import com.mediaplus.server.io.imdb.MovieInfo;
import com.mediaplus.server.util.Constants;
import uk.co.caprica.vlcj.filter.AudioFileFilter;
import uk.co.caprica.vlcj.filter.VideoFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class FileIO {
	public FileIO() {
		
	}

    public String[] readFile(String fileUrl) {
		try {
			Path path = Paths.get(fileUrl);
			
			Object[] objectArray = Files.readAllLines(path, StandardCharsets.UTF_8).toArray();

            return Arrays.copyOf(objectArray, objectArray.length, String[].class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new String[0]; // return a blank string array
	}
	
	public String[] listFilesInFolder(String url) {
		String[] contents = listContentsInFolder(url);
		ArrayList<String> files = new ArrayList<String>();
		
		for (String fileUrl : contents) {
			File file = new File(fileUrl);
			
			if (!file.isDirectory()) {
				files.add(file.getName());
			}
		}

        return ArrayListToStringArray(files);
	}
	
	public boolean fileExists(String fileUrl) {
		File f = new File(fileUrl);

        return f.exists() && !f.isDirectory();

    }
	
	public boolean folderExists(String folderUrl) {
		File f = new File(folderUrl);

        return f.exists() && f.isDirectory();

    }
	
	public boolean createNewFile(String fileUrl) {
		try {
			File file = new File(fileUrl);
			return file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean createNewFolder(String folderUrl) {
		File folder = new File(folderUrl);
		
		return folder.mkdirs();
	}

    @SuppressWarnings("unused")
	public boolean writeToFile(String fileUrl, boolean append, String line) {
		try {
		    FileWriter fw = new FileWriter( fileUrl, append);
		    fw.write(line + "\n");

		    fw.flush();
		    fw.close();

		    return true;
		} catch(IOException e) {
			e.printStackTrace();
		}

	    return false;
	}
    
	public boolean writeToFile(String fileUrl, boolean append, String[] lines) {
		try {
		    FileWriter fw = new FileWriter( fileUrl, append);

		    for (String line : lines) {
		    	fw.write(line + "\n");
		    }

	    	fw.flush();
		    fw.close();
		    
		    return true;
		} catch(IOException e) {
			e.printStackTrace();
		}
		
	    return false;
	}
	
	// Private
	private String[] listContentsInFolder(String url) {
		File file = new File(url);
		File folder = new File(file.getAbsolutePath());
		
		File[] files = folder.listFiles();
        assert files != null;
        String[] fileNames = new String[files.length];
		
		for (int i=0; i<files.length; i++) {
			fileNames[i] = files[i].getName();
		}
	    
	    return fileNames;
	}
	
	private String[] ArrayListToStringArray(ArrayList<String> arrayList) {
		String[] stringArray = new String[arrayList.size()];
		
		stringArray = arrayList.toArray(stringArray);
		
		return stringArray;
		
	}

    public static File[] getFilms(){
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\films");

        FileFilter filter = new VideoFileFilter();
        File[] files = folder.listFiles(filter);

        MovieInfo info = new MovieInfo();
        for (File file : files) {
            info.getFilm(file.getName());
        }

        return files;
    }

    public static String stripExtension(String fileName){
        int index = fileName.lastIndexOf(".");

        if(index > 0){
            fileName = fileName.substring(0, index);
        }

        return fileName;
    }

    public static int getNoFilms(){
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\films");
        FileFilter filter = new VideoFileFilter();
        File[] files = folder.listFiles(filter);
        return files.length;
    }
    public static File[] getImages() {
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\pictures");
        FileFilter filter = new ImageFileFilter();
        File[] files = folder.listFiles(filter);

        return files;
    }

    public static File[] getMusic() {
        File folder = new File(Constants.MOVIE_ROOT_DIR + "\\music");

        FileFilter filter = new AudioFileFilter();
        File[] files = folder.listFiles(filter);

        MovieInfo info = new MovieInfo();
        for (File file : files) {
            info.getFilm(file.getName());
        }

        return files;
    }

}
